<?php

function readResponse($fp) {
    $output = "";
    $count = 0;
    do {
        $char = fgetc($fp);
        $output .= $char;
        switch ($char) {
        case "{":
            $count += 1;
            break;
        case "}":
            $count -= 1;
            break;
        }
    } while ($count > 0);
    return $output;
}

header('Content-type: application/json');

$obj = json_decode($HTTP_RAW_POST_DATA, true);

$json = json_encode($obj['data']);

$fp = fsockopen($obj['target'], 9090);
/*
$desc = array(
    0 => array("pipe", "r"),
    1 => array("pipe", "w")
);
//$fp = proc_open("telnet -l xbmc ".$obj['target']." 9090", $desc, $pipes);
 */

fputs($fp,$json);
//print fgets($pipes[1], 50);
$response = readResponse($fp);
fclose($fp);

print $response;

?>
