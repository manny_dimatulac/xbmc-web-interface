angular.module("xbmc_cmds", []).factory('xbmc_cmds', function () {
	var service = {};

	var MAX_ID = 128;

	var IDGen = {
		current: 0,
		MAX: MAX_ID,
		getId: function () {
			var new_id = this.current;
			this.current += 1;
			this.current %= this.MAX;
			return new_id;
		}
	};

	var XBMCCommand = function (id, content) {
		var command = {"jsonrpc": "2.0", "id": id};
		angular.forEach(content, function (obj, label) {
			command[label] = obj;
		});
		return command;
	};

	service.json_playpause = function (player) {
		return XBMCCommand(IDGen.getId(), {
			"method": "Player.PlayPause",
			"params": {
				"playerid": player,
			},
		});
	};

	service.json_stop = function (player) {
		return XBMCCommand(IDGen.getId(), {
			"method": "Player.Stop",
			"params": {
				"playerid": player,
			},
		});
	};

	service.json_goto = function (player, pos) {
		return XBMCCommand(IDGen.getId(), {
			"method": "Player.GoTo",
			"params": {
				"playerid": player,
				"to": pos
			},
		});
	};

	service.json_getactiveplayers = function () {
		return XBMCCommand(IDGen.getId(), {
			"method": "Player.GetActivePlayers"
		});
	};

	service.json_getcurrentitem = function (player) {
		return XBMCCommand(IDGen.getId(), {
			"method": "Player.GetItem", 
			"params": { 
				"playerid": player,
            //"properties": ["title", "artist"]
        }
    });
	};
	service.json_getproperties = function (player, properties) {
		return XBMCCommand(IDGen.getId(), {
			"method": "Player.GetProperties",
			"params": { 
				"playerid": player,
				"properties": properties,
			}
		});
	};

	service.json_getmediasources = function (media) {
		return XBMCCommand(IDGen.getId(), {
			"method": "Files.GetSources",
			"params": {
				"media": media,
			}
		});
	};

	service.json_getdirectory = function (dir, media) {
		if (media == undefined) {
			media = "files";
		}
		return XBMCCommand(IDGen.getId(), {
			"method": "Files.GetDirectory",
			"params": {
				"directory": dir,
				"media": media,
				"sort": {
					"method": "file"
				}
			}
		});
	};

	service.json_playeropen = function (item) {
		var obj = {};
		obj[item.filetype] = item.file;
		return XBMCCommand(IDGen.getId(), {
			"method": "Player.Open",
			"params": {
				"item": obj
			}
		});
	};

	service.json_getplaylists = function () {
		return XBMCCommand(IDGen.getId(), {
			"method": "Playlist.GetPlaylists",
		});
	};

	service.json_playlistadd = function (playlist, item) {
		var obj = {};
		obj[item.filetype] = item.file;
		return XBMCCommand(IDGen.getId(), {
			"method": "Playlist.Add",
			"params": {
				"playlistid": playlist,
				"item": obj,
			}
		});
	};

	service.json_playlistgetitems = function (playlist) {
		return XBMCCommand(IDGen.getId(), {
			"method": "Playlist.GetItems",
			"params": {
				"playlistid": playlist
			}
		});
	};
	return service;
});