<?php

header('Content-Type: application/json');

$obj = json_decode($HTTP_RAW_POST_DATA, true);

$user = $obj["user"];
$host = $obj["host"];
$port = $obj["port"];
$data = json_encode($obj["data"]);

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_URL, "http://".$user."@".$host.":".$port."/jsonrpc");
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

print curl_exec($ch);


?>
