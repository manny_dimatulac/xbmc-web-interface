var HOST = 'ws://192.168.1.101:9090/jsonrpc';

angular.module('XBMCConnection', []).
factory('xbmc_connection', function () {
	var events = {
		SOCKET_READY: 		0,
		SOCKET_NOT_READY: 	1,
		DATA_RECEIVED: 		2,
	}
	var UNIQUE_ID = 0;

	// define Listener interface
	var Listener = function () {};
	Listener.prototype.notify = function (event, data) {};

	var Connection = function (id) {
		this.id = id;
	};
	Connection.prototype.sendJson = function (json) {
		send(this.id, json);
	};

	var socket = new WebSocket(HOST);
	socket.onopen = function (evt) {
        active = true;
        broadcast(events.SOCKET_READY);
        //updateState();
    };
    socket.onclose = function (evt) {
        active = false;
        broadcast(events.SOCKET_NOT_READY);
    };
    socket.onmessage = function (evt) {
    	var json = $.parseJSON(evt.data);
    	if ('id' in json) {
    		var subscriber = json.id.split('.')[0];
    		json.id = json.id.substring(subscriber.length+1);
    		publish(subscriber, events.DATA_RECEIVED, json);
    	} else {
    		broadcast(events.DATA_RECEIVED, json);
    	}
    };
    var send = function (id, json) {
    	json.id = '' + id + "." + json.id;
    	socket.send(JSON.stringify(json));
    };
	var active = false;
	var subscribers = {};
	var subscribe = function (listener) {
		var id = UNIQUE_ID++;
		var conn = new Connection(id);
		subscribers[id] = conn;
		if (listener) {
			conn.notify = listener;
		} else {
			conn.notify = new Listener();
		}
		if (active) {
			publish(id, events.SOCKET_READY);
		}
		return conn;
	};
	var publish = function (id, event, data) {
		if (id in subscribers) {
			subscribers[id].notify(event, data);
		} else {
			console.log("unknown id:", id);
		}
	}
	var broadcast = function (event, data) {
		for (id in subscribers) {
			publish(id, event, data);
		}
	}
	return {
		events: events,
		subscribe: subscribe,
	}
});