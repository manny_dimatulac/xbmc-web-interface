XbmcCommand = function (obj) {
	this.id = 0;
	this.jsonrpc = "2.0";
	this.payload = {};
	this.setPayload(obj);
};

XbmcCommand.prototype.setId = function (id) {
	this.id = id;
};

XbmcCommand.prototype.getId = function () {
	return this.id;
};

XbmcCommand.prototype.setPayload = function (obj) {
	for (key in obj) {
		if (obj.hasOwnProperty(key)) { 
			this.payload[key] = obj[key];
		}
	}
};

XbmcCommand.prototype.getPayload = function () {
	return this.payload;
};

XbmcCommand.prototype.toObject = function () {
	var obj = {
		jsonrpc: this.jsonrpc,
		id: this.id,
	};
	for (key in this.payload) {
		if (this.payload.hasOwnProperty(key)) {
			obj[key] = this.payload[key];
		}
	}
	return obj;
};

XbmcCommand.prototype.toJson = function () {
	return JSON.stringify(this.toObject());
}