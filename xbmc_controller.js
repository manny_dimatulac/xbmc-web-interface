var App = angular.module("App", ["xbmc_cmds", "XBMCConnection"]);
var asus_lan = "192.168.1.101";
var ID = "Manny";

App.factory('xbmc_service', function ($rootScope, xbmc_cmds, xbmc_connection) {
    var service = {
        playerState: {
            isPlaying: false,
            currentItem: {},
            activePlayer: null,
            activeTitle: "",
        }
    };

    var callbacks = {};

    // [id]: {timeout, retries, command}
    var sends = {};

    var socketActive = false;
    var MAX_RETRIES = 5;
    var TIMEOUT_MS = 10000;

    var connectionHandler = function (event, data) {
        var events = xbmc_connection.events;
        switch (event) {
            case events.SOCKET_READY:
            socketActive = true;
            $rootScope.$broadcast("socketReady");
            break;
            case events.SOCKET_NOT_READY:
            socketActive = false;
            break;
            case events.DATA_RECEIVED:
            handleMessage(data);
            break;
        }
    }

    var connection = xbmc_connection.subscribe(connectionHandler);

    var timeout_handler = function (id) {
        sends[id].retries++;
        if (sends[id] >= MAX_RETRIES) {
            console.log("error: reached max retries for", sends[id].command);
        } else {
            send(sends[id].command, callbacks[id]);
        }
    };

    var send = function (command, callback) {
        var id = command.id;
        var msg = JSON.stringify(command);
        if (socketActive) {
            connection.sendJson(command);
            sends[id] = {
                timeout: setTimeout(function () { timeout_handler(id); }, TIMEOUT_MS),
                retries: 0,
                command: command
            };
            if (callback) {
                callbacks[id] = callback;
            }
            console.log("sending:", command);
        } else {
            console.log("Error: Could not send message: " + msg);
        }
    };

    var handleMessage = function (json) {
        //var json = $.parseJSON(data);
        console.log("received:", json);
        if (json.id != undefined) {
            // cancel retry
            if (sends[json.id]) {
                clearTimeout(sends[json.id].timeout);
                delete sends[json.id];
                var f = callbacks[json.id];
                if (f != undefined)  {
                    f(json);
                }
            } else {
                console.log("Repeat:", json.id);
            }
        } else {
            if (json.method) {
                /*
                if (json.method == "Player.OnPlay" || json.method == "Player.OnPause") {
                    broadcastPlayerUpdate(json.params.data.player.playerid);
                }
                */
                broadcastNotification(json.method, json.params);
            }
        }
    };

    var broadcastNotification = function (method, params) {
        $rootScope.$broadcast("notification", method, params);
    };

    var broadcastPlayerUpdate = function (id) {
        console.log("broadcast");
        $rootScope.$broadcast("playerUpdate", id);
    };

    var pause = function (player) {
        send(xbmc_cmds.json_playpause(player));
    };

    var stop = function (player) {
        send(xbmc_cmds.json_stop(player));
    };

    var prev = function (player) {
        send(xbmc_cmds.json_goto(player, "previous"));
    };

    var next = function (player) {
        send(xbmc_cmds.json_goto(player, "next"));
    };

    var sendWithCallback = function (msg, callback) {
        if (callback) {
            send(msg, function (resp) {
                $rootScope.$apply(callback(resp.result));
            });
        } else {
            send(msg);
        }
    };

    var getActivePlayers = function (callback) {
        sendWithCallback(xbmc_cmds.json_getactiveplayers(), callback);
    };

    var getTitle = function (player, callback) {
        sendWithCallback(xbmc_cmds.json_getcurrentitem(player), callback);
    };

    var isPlaying = function (player, callback) {
        sendWithCallback(xbmc_cmds.json_getproperties(player, ["speed"]), callback);
    };

    var getDirectory = function (dir, media, callback) {
        sendWithCallback(xbmc_cmds.json_getdirectory(dir, media), callback);
    };

    var getSources = function (type, callback) {
        sendWithCallback(xbmc_cmds.json_getmediasources(type), callback);
    };

    var playItem = function (item, callback) {
        sendWithCallback(xbmc_cmds.json_playeropen(item), callback);
    };

    var getPlaylists = function (callback) {
        sendWithCallback(xbmc_cmds.json_getplaylists(), callback);
    };

    var playlistAdd = function (playlist, item, callback) {
        sendWithCallback(xbmc_cmds.json_playlistadd(playlist, item), callback);
    };

///////////////////////// service definitions

    service.pause = pause;
    service.stop = stop;
    service.prev = prev;
    service.next = next;
    service.getActivePlayers = getActivePlayers;
    service.getTitle = getTitle;
    service.isPlaying = isPlaying;
    service.getSources = getSources;
    service.getDirectory = getDirectory;
    service.playItem = playItem;
    service.getPlaylists = getPlaylists;
    service.playlistAdd = playlistAdd;
    service.enqueue = function (type, item, callback) {
        var id = 0;
        switch (type) {
            case "music":
            case "audio":
                id = 0;
                break;
            case "video":
                id = 1;
                break;
        }
        playlistAdd(id, item, callback);
    };

    service.getPlaylistItems = function (playlist, callback) {
        sendWithCallback(xbmc_cmds.json_playlistgetitems(playlist), callback);
    };

    return service;
});

App.controller("player", function ($scope, xbmc_service) {
    var activePlayers = [];

    var updateTitle = function (player) {
        xbmc_service.getTitle(player, function (result) {
            var item = result.item;
            $scope.activeTitle = item.label;
        });
    };

    var initPlayer = function (player) {
        console.log("init");
        $scope.activeTitle = "";
        $scope.isPlaying = false;
    };

    var updatePlayer = function (id) {
        console.log(id);
        $scope.player = id;
        updateTitle(id);
        xbmc_service.isPlaying(id, function (result) {
            $scope.isPlaying = result.speed == 1;
        });
    };

    var updatePlayers = function () {
        console.log("getting active");
        xbmc_service.getActivePlayers(function (players) {
            activePlayers = players;
            angular.forEach(players, function (player) {
                updatePlayer(player.playerid);
            });
        });
    };

    $scope.$on("socketReady", updatePlayers);
    $scope.$on("playerUpdate", function (event, id) { updatePlayer(id); });
    $scope.$on("notification", function (event, method, params) {
        switch (method) {
            // TODO: refine this more
            case "Player.OnPlay":
            case "Player.OnPause":
                updatePlayers();
                break;
            case "Player.OnStop":
                initPlayer();
                break;
        }
        $scope.$digest();
    });

    $scope.isPlaying = false;
    $scope.playpause = function () {
        xbmc_service.pause($scope.player);
    };
    $scope.stop = function () {
        xbmc_service.stop($scope.player);
    };
    $scope.prev = function () {
        xbmc_service.prev($scope.player);
    };
    $scope.next = function () {
        xbmc_service.next($scope.player);
    };
    initPlayer();
});

App.controller('browser', function ($scope, xbmc_service) {
    var sources = [];
    $scope.cwd = [];
    $scope.crumbs = [{
            source: {label: "Sources"}, 
            files: []
    }];
    $scope.loading = false;
    $scope.type = "files";
    $scope.$on("socketReady", function () {
        /*
        angular.forEach(getSources($scope.type), function (source) {
            $scope.crumbs[0].files.push(source);
        });
*/
        $scope.crumbs[0].files = getSources($scope.type);
    });

    $scope.setAudioCwd = function (source) {
    };
    var getDirectory = function (path, media, callback) {
        xbmc_service.getDirectory(path, media, callback);
    };

    var getSources = function (type) {
        var sources = [];
        //$scope.crumbs = [];
        $scope.loading = true;
        xbmc_service.getSources(type, function (result) {
            //audioSources = result.sources;
            angular.forEach(result.sources, function (source) {
                sources.push(source);
            });
            $scope.loading = false;
        });
        return sources;
    };

    var getSource = function (source, type) {
        var newSource = {source: source, files: []};
        $scope.loading = true;

        var dir = "";

        // figure out the directory since the source object might not
        // be accurate
        if ($scope.crumbs.length > 1) {
            dir += $scope.crumbs[1].source.file; 
            for (var i = 2; i < $scope.crumbs.length; ++i) {
                dir += '/' + $scope.crumbs[i].source.label;
            }
            dir += '/' + source.label;
        } else {
            dir = source.file;
        }

        getDirectory(dir, type, function (result) {
            angular.forEach(result.files, function (file) {
                newSource.files.push(file);
            });
            $scope.loading = false;
        });
        return newSource;
    };

    $scope.setCrumb = function (index) {
        $scope.crumbs = $scope.crumbs.slice(0, index+1);
    };

    $scope.addCrumb = function (source) {
        $scope.searchText = "";
        $scope.crumbs.push(getSource(source, $scope.type));
    };

    $scope.getCwd = function () {
        return $scope.crumbs[$scope.crumbs.length-1].files;
    };

    $scope.playItem = function (source) {
        xbmc_service.playItem(source);
    };
    $scope.enqueue = function (type, source) {
        xbmc_service.enqueue(type, source, function (result) {
            console.log(result);
        });
    };
    $scope.searchText = "";
});

App.controller('tabs', function ($scope) {
    var active = "audio";

    $scope.getActive = function () {
        return active;
    };

    $scope.setActive = function (genre) {
        active = genre;
    };

    $scope.genres = {
        "audio": {
            "tablabel": "Audio",
            "type": "music"
        },
        "video": {
            "tablabel": "Video",
            "type": "video"
        }
    };
});

App.controller('playlist', function ($scope, xbmc_service) {
    $scope.items = [];

    var getPlaylistItems = function (playerid) {
        var out = [];
        xbmc_service.getPlaylistItems(playerid, function (items) {
            angular.forEach(items.items, function (item) { out.push(item); });
        });
        return out;
    };

    var updatePlaylist = function () {
        xbmc_service.getActivePlayers(function (players) {
            angular.forEach(players, function (player) {
                $scope.items = getPlaylistItems(player.playerid);
            });
        });
    };

    $scope.$on("socketReady", updatePlaylist);

    $scope.$on("notification", function (event, method, params) {
        if (method.substring(0, 8) == "Playlist") {
            console.log("updating playlist");
            updatePlaylist();
        }
    });
});
